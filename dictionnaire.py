from hashlib import md5
import sys, time, re

def hashmd5(to_hash) :
    string = ''.join(to_hash)
    hashed = md5(string.encode())
    return hashed.hexdigest()

dict_path = sys.argv[2]
file_path = sys.argv[1]
start_time=time.time()

with open(file_path, "r") as shadow :
    lines = shadow.readlines()
    hashs = {}
    for line in lines:
        try:
            user, user_hash = re.findall('^([^:]+):\$1\$([^:]+):.+', line)[0]
        except IndexError:
            continue  
        else:
            hashs[user]=user_hash

with open(dict_path, "r") as dictionary :
    to_test = []
    to_test = dictionary.readlines()

result = {}

for word in to_test :
    word = word.strip()
    generated_hash = hashmd5(word)
    for username, hash_passwd in hashs.items():
        if generated_hash == hash_passwd :
            result[username] = word

with open('out_dict.txt', 'w') as out_file :
    find = result.keys()
    for user in find :
        print('Le mot de passe du user', user, 'est :', result[user], file=out_file)
        print('-- en %s secondes --' % (time.time() - start_time),file=out_file)