from hashlib import md5
import itertools
import sys
import time

def hashmd5(to_hash) :
    string = ''.join(to_hash)
    hashed = md5(string.encode())
    return hashed.hexdigest()

chars = "abcdefghijklmnopqrstuvwxyz"
lenght = int(sys.argv[2])
file_path = sys.argv[1]
start_time=time.time()

with open(file_path, "r") as shadow :
    hashs = dict()
    for line in shadow :
        username, hash_passwd, *trash = line.split(":")
        if hash_passwd not in ('*', '!'):
            hashs[hash_passwd[3:]] = username  # remove $1$

with open('out.txt', 'w') as out_file :
    for word in itertools.product(chars, repeat=lenght) :
        generated_hash = hashmd5(word)
        print(word) # premet de voir en temps réel l'avancer
        if generated_hash in hashs :
            print('Le mot de passe du user', hashs[generated_hash], 'est :', word, file=out_file)
            print('-- en %s secondes --' % (time.time() - start_time),file=out_file) 
