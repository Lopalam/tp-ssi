# TP - Attaque sur les mots de passes

**Par Sacha Vilallonga et Valentin Lachamp**

## 3.1 Étude du contenu du fichier shadow

### 3.1.1 Rappels théoriques

Le fichier `/etc/shadow` contient les mots de passes et les informations liés aux utilisateurs. Il a la forme suivante : 
```
root:$1$934b4a210c17493f68bf6bfe74bff77a:16749:0:99999:7:::
fred:$1$9ebf8e708dcb3f28cb43d5d52655ab14:16561:0:99999:7:::
mysql:!:16939:0:99999:7:::
uuidd:*:16940:0:99999:7:::
giselle:$1$6e5fa4d9c48ca921c0a2ce1e64c9ae6f:17078:0:99999:7:::
libvirt-qemu:!:17105:0:99999:7:::
```

Les `:` séparent les lignes en différents champs qui représentent réspectivement :

- Le nom d'utilisateur (max 8 caractères).
- L'empreinte du mot de passe (13 caractères chiffrés). Un champ vide indique que le mot de passe n'est pas demander à la connexion.
- Le nombre de jours entre le 1er janvier 1970 et le dernier changement du mot de passe.
- Le nombre de jour avant que le mot de passe ne puisse être changer. Un 0 indique que le mot de passe peut être changer n'importe quand.
- Le nombre de jours avant que le mot de passe ne doivent être changer.
- La durée (en jour) pendant laquelle l'utilisateur sera averti avant que le mot de passe n'expire.
- Le nombre de jours avant la désactivation du compte suite à l'expiration du mot de passe.
- La date d'expiration du compte, exprimé en nombre de jours depuis le 1er janvier 1970.
- Réservé pour une utilisation future.

Les empreintes des mots de passes sont généré avec l'algorythme `MD5`.

## 3.2 Mise en œuvre d'un script d'attaque par force brute

### 3.2.1 Rappels théorique

Une attaque par force brute consiste à tester chaque caractère l'un après l'autre afin de déterminer les identifiants de connexion par exemple.

### 3.2.2 Mise en œuvre du script

Nous avons mis en place un script en python qui test toutes les combinaisons des caractères suivants : `abcdefghijklmnopqrstuvwxyz`
> *Disponible en annexe*

Nous n'avons trouver qu'un seul mot de passe :
```
Le mot de passe du user giselle est : ('b', 'r', 'a', 'z', 'i', 'l')
-- en 119.40819907188416 secondes --
```

> Pour en trouver plus nous aurions pu utilisé le jeu de caractères suivant : `abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789&-_@=)`

## 3.3 Mise en œuvre d'un script d'attaque par dictionnaire

### 3.3.1 Rappels théoriques

Une attaque par force dictionnaire consiste à tester des mots de passe connus les uns après les autres, afin de déterminer des identifiants par exemple.

### 3.3.2 Mise en œuvre du script

Nous avons mis en place un script en python qui test tous les mots de passes contenu dans le dictionnaire fourni.
> *Disponible en annexe*

Nous n'avons trouver que deux mots de passe :
```
Le mot de passe du user giselle est : brazil
-- en 0.019179105758666992 secondes --
Le mot de passe du user root est : t3_p@hEN_v12
-- en 0.019192218780517578 secondes --
```

> Pour en trouver plus nous aurions pu utilisé un dictionnaire plus fournit.

## Avantages et inconvénients des deux méthodes

L'attaque par `force brute` a un taux de réussite de 100% car toutes les possibiltés vont être tester cependant seul le temps va déterminer la faisabilité d'une attaque par `force brute`.

L'attaque par `dictionnaire` ne retournera pas forcément un résultat positif. L'attaque par dictionnaire dépend de la présence du mot de passe recherché au sein du dictionnaire utilisé. C'est une attaque qui est plus rapide et qui s'appuie sur la faiblesse humaine qui est incapable de retenir plusieurs mots de passes complexes différents.  

## Annexe

### Script attaque par bruteforce en Python 3 :

```python 
from hashlib import md5
import itertools
import sys
import time

def hashmd5(to_hash) :
    string = ''.join(to_hash)
    hashed = md5(string.encode())
    return hashed.hexdigest()

chars = "abcdefghijklmnopqrstuvwxyz"
lenght = int(sys.argv[2])
file_path = sys.argv[1]
start_time=time.time()

with open(file_path, "r") as shadow :
    hashs = dict()
    for line in shadow :
        username, hash_passwd, *trash = line.split(":")
        if hash_passwd not in ('*', '!'):
            hashs[hash_passwd[3:]] = username  # remove $1$

with open('out.txt', 'w') as out_file :
    for word in itertools.product(chars, repeat=lenght) :
        generated_hash = hashmd5(word)
        print(word) # premet de voir en temps réel l'avancer
        if generated_hash in hashs :
            print('Le mot de passe du user', hashs[generated_hash], 'est :', word, file=out_file)
            print('-- en %s secondes --' % (time.time() - start_time),file=out_file) 

```

### Script attaque par dictionnaire en Python 3 : 

```python 
from hashlib import md5
import sys, time, re

def hashmd5(to_hash) :
    string = ''.join(to_hash)
    hashed = md5(string.encode())
    return hashed.hexdigest()

dict_path = sys.argv[2]
file_path = sys.argv[1]
start_time=time.time()

with open(file_path, "r") as shadow :
    lines = shadow.readlines()
    hashs = {}
    for line in lines:
        try:
            user, user_hash = re.findall('^([^:]+):\$1\$([^:]+):.+', line)[0]
        except IndexError:
            continue  
        else:
            hashs[user]=user_hash

with open(dict_path, "r") as dictionary :
    to_test = []
    to_test = dictionary.readlines()

result = {}

for word in to_test :
    word = word.strip()
    generated_hash = hashmd5(word)
    for username, hash_passwd in hashs.items():
        if generated_hash == hash_passwd :
            result[username] = word

with open('out_dict.txt', 'w') as out_file :
    find = result.keys()
    for user in find :
        print('Le mot de passe du user', user, 'est :', result[user], file=out_file)
        print('-- en %s secondes --' % (time.time() - start_time),file=out_file)
```